terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }

  backend "s3" {
    bucket = "treylle-terraform-bucket-srv2b1k5s3"
    key    = "terraform"
    region = "ap-southeast-1"
  }
}

provider "aws" {
  region  = "ap-southeast-1"
}

variable "projectname" {
  type        = string
  default     = "treylle"
  description = "a path social media clone"
}

variable "projectversion" {
  type        = string
  default     = "srv2b1k5s3"
  description = "project version value"
}

resource "aws_dynamodb_table" "treylledynamodb" {
  name           = "treylle-dynamodb-${var.projectversion}"
  billing_mode   = "PROVISIONED"
  read_capacity  = 5
  write_capacity = 5
  hash_key       = "partitionKey"
  range_key      = "sortKey"

  attribute {
    name = "partitionKey"
    type = "S"
  }

  attribute {
    name = "sortKey"
    type = "S"
  }

  tags = {
    projectname = var.projectname
  }
}

resource "aws_s3_bucket" "treyllebucket" {
  bucket = "treylle-bucket-${var.projectversion}"

  tags = {
    projectname = var.projectname
  }
}

resource "aws_sqs_queue" "treyllequeue" {
  name                      = "treylle-queue-${var.projectversion}"
  delay_seconds             = 0
  max_message_size          = 2048
  message_retention_seconds = 86400
  receive_wait_time_seconds = 0

  tags = {
    projectname = var.projectname
  }
}

resource "aws_iam_policy" "treylleiampolicy" {
  name        = "treylle-iampolicy-${var.projectversion}"
  description = "Treylle Iam Policy"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "dynamodb:Query",
          "dynamodb:Scan",
          "dynamodb:DeleteItem",
          "dynamodb:UpdateItem",
          "dynamodb:PutItem",
          "s3:PutObject",
          "s3:GetObject",
          "s3:DeleteObject",
          "sqs:SendMessage",
          "sqs:DeleteMessage",
          "sqs:SendMessageBatch",
          "sqs:ReceiveMessage",
          "sqs:GetQueueUrl",
          "sqs:GetQueueAttributes",
          "sqs:PurgeQueue"
        ]
        Resource = [
          aws_dynamodb_table.treylledynamodb.arn,
          aws_s3_bucket.treyllebucket.arn,
          aws_sqs_queue.treyllequeue.arn
        ]
        Effect = "Allow"
      },
    ]
  })
  
  tags = {
    projectname = var.projectname
  }
}

resource "aws_iam_role" "treylleiamrole" {
  name = "treylle-iamrole-${var.projectversion}"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "tasks.apprunner.amazonaws.com"
        }
      },
    ]
  })

  tags = {
    projectname = var.projectname
  }
}

resource "aws_iam_role_policy_attachment" "treylleiamrolepolicyattachment" {
  role       = aws_iam_role.treylleiamrole.name
  policy_arn = aws_iam_policy.treylleiampolicy.arn
}

resource "aws_sqs_queue_policy" "treyllequeuepolicy" {
  queue_url = aws_sqs_queue.treyllequeue.id
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "sqs:SendMessage",
          "sqs:DeleteMessage",
          "sqs:SendMessageBatch",
          "sqs:ReceiveMessage",
          "sqs:GetQueueUrl",
          "sqs:GetQueueAttributes",
          "sqs:PurgeQueue"
        ]
        Effect = "Allow"
        Sid    = ""
        Principal = {
          AWS = [
            aws_iam_role.treylleiamrole.arn,
            "arn:aws:iam::933153878921:user/villain2023cliadmin"
          ]
        }
      },
    ]
  })
}
